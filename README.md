# Tasks

Repositorio semilla para trabajar en el [Workshop: CI/CD con GitLab y Google App Engine](https://www.eventbrite.com/e/entradas-workshop-online-cicd-con-gitlab-y-google-app-engine-58265159661?aff=DiegoHerrera) de 9 de mayo de 2019. Este workshop se ofrece como introducción al [Full Stack DevOps Bootcamp](https://keepcoding.io/es/bootcamp-devops/) de KeepCoding. Si quieres profundizar más en la cultura DevOps y cómo se aplica en casos reales, solo tienes que apuntarte al bootcamp :-)
